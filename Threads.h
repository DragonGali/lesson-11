#include <string>
#include <fstream>
#include <iostream>
using namespace std;
#include<vector>
#include <thread>
#include <chrono>
using namespace std::chrono;
#include <iomanip>



void I_Love_Threads();
void call_I_Love_Threads();

void printVector(std::vector<int> primes);

void getPrimes(int begin, int end, std::vector<int> primes);
std::vector<int> callGetPrimes(int begin, int end);


void writePrimesToFile(int begin, int end, ofstream& file);
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N);
