#include "Threads.cpp"

#define NUM_OF_THREADS 10


/*

	DESCRIPTION:
	-----------

	this program tests out different kind of ways to use threads.

*/

int main()
{
	//Part 1
	call_I_Love_Threads();

	//Part 2 - section A  
	vector<int> primes1;
	getPrimes(58, 100, ref(primes1));// i added ref so that the previous vector gets changed.
	printVector(primes1);
	printVector(callGetPrimes(58, 100));

	//Part 2 - section B
	callGetPrimes(0, 1000);
	callGetPrimes(0, 1000000);
	callGetPrimes(0, 100000000);// this part might take some time if you lunch this all together so if you dont see part 3 dont worry it works.
	                            // you should just lunch them separatly.

	//Part 3 - section C
	std::cout << NUM_OF_THREADS << " threads are running " << std::endl;
	callWritePrimesMultipleThreads(0, 1000, "primes2.txt", NUM_OF_THREADS);
	callWritePrimesMultipleThreads(0, 100000, "primes2.txt", NUM_OF_THREADS);
	callWritePrimesMultipleThreads(0, 1000000, "primes2.txt", NUM_OF_THREADS);

	//std::cout << "\nOnly one thread are running " << std::endl;
	callWritePrimesMultipleThreads(0, 1000, "primes2.txt", 1);
	callWritePrimesMultipleThreads(0, 100000, "primes2.txt", 1);
	callWritePrimesMultipleThreads(0, 1000000, "primes2.txt", 1);


	system("pause");
	return 0;
}

