#include "Threads.h";


inline void I_Love_Threads() // prints I Love Threads
{
	cout << "I Love Threads\n";
}

inline void call_I_Love_Threads()// creates a thread
{
	thread t1(I_Love_Threads);
	t1.join();
}

inline void printVector(std::vector<int> primes)// prints a vector
{
	vector<int>::iterator it;
	for (it = primes.begin(); it != primes.end(); ++it)
	{
		cout << *it << std::endl;
		cout << "\n";
	}

}

inline void getPrimes(int begin, int end, std::vector<int> primes)// finds all the primenumbers in a given range and pushes them into a vector.
{
	bool check = true;

	for (int i = begin; i <= end; i++)
	{
		check = true;

		if (i <= 1)
			check = false;

		// Check from 2 to n-1
		for (int j = 2; j < i; j++)
			if (i % j == 0)
				check = false;

		if(check)
			primes.push_back(i);
	}

	printVector(primes);

}

inline std::vector<int> callGetPrimes(int begin, int end)// creates a new threads and calls the primes function.
{
	vector<int> primes;
	thread t2(getPrimes,begin, end, ref(primes));
	this_thread::sleep_for(1s);// time to cool down
	t2.join();
	return primes;
}

inline void writePrimesToFile(int begin, int end, ofstream& file)// uses callgetprimes to write the primes in a given range into a file
{

	vector<int> primes = callGetPrimes(begin, end);
	for (int i = 0; i < primes.size(); i++)
	{
		file << primes[i];
		file << "\n";
	}

}

inline void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)// creates a thread vector and checks thread time.
{
	ofstream file;
	file.open(filePath);
	vector<thread> mythreads;


	int i = 0;
	auto start = high_resolution_clock::now();// starting clock

	for (i = 0; i < N; i++)
	{
		mythreads.push_back(thread(writePrimesToFile, begin, end, ref(file)));
	}

	for (i = 0; i < N; i++)
	{
		mythreads[i].join();
	}

	auto stop = high_resolution_clock::now();// stopping clock
	auto duration = duration_cast<seconds>(stop - start);
	file.close();
	cout << N << " Threads took: " << duration.count() << " seconds" << endl;

}